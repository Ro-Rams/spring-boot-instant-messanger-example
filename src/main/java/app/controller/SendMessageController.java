package app.controller;

import app.model.Message;
import app.model.Messages;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendMessageController {

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping("/sendMessage")
    public void sendMessage(@RequestParam String user, @RequestParam String message) {
        Messages.addMessage(new Message(user, message));
    }
}
