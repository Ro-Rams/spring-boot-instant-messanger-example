package app.controller;

import app.model.Messages;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetMessagesController {

    @CrossOrigin(origins = "http://localhost:9000")
    @RequestMapping("/getMessages")
    public String getMessages() {
        return Messages.getMessages();
    }
}
