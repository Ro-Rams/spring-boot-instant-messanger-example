package app.model;

import java.time.LocalDateTime;

public class Message {

    private final String user;
    private final String message;
    private final LocalDateTime timestamp;

    public Message(final String user, final String message) {
        this.user = user;
        this.message = message;
        this.timestamp = LocalDateTime.now();
    }

    public final String getMessage() {
        return getTimestamp() + user + " : " + message;
    }

    private final String getTimestamp() {
        return "(" + timestamp.toLocalDate().toString() + ":" + timestamp.toLocalTime().toString() + ") - ";
    }
}
