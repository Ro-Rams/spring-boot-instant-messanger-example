package app.model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Messages {

    private static final List<Message> messages = new CopyOnWriteArrayList<>();

    public static final String getMessages() {
        if (messages.isEmpty()) {
            return "";
        } else if (messages.size() == 1) {
            return messages.get(0).getMessage();
        } else {
            StringBuilder messagesForUI = new StringBuilder();
            for (int x = 0; x < messages.size() - 1; x++) {
                messagesForUI.append(messages.get(x).getMessage())
                                .append("\n");
            }
            messagesForUI.append(messages.get(messages.size() - 1).getMessage());
            return messagesForUI.toString();
        }
    }

    public static final void addMessage(final Message message) {
        messages.add(message);
    }
}
