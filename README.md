# spring-boot-instant-messanger-example

An instant messanger Spring Boot web app example.

## Running Locally

- Import the project (as a gradle project if possible) into your favourite IDE
- Run the gradle script
- Run the main method in Application.java from your IDE

Your app should now be running on [localhost:8000](http://localhost:8000/).