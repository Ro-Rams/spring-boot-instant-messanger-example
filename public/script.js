var HEROKU = 'https://tech-club-spring-boot-example.herokuapp.com/'
var LOCAL = 'http://localhost:9000/'
var host = LOCAL

$(document).ready(function() {
	pressButtonOnEnter('#user-message', '#send-message')
	pressButtonOnEnter('#username', '#new-user')
	callGetMessagesEvery500Millis()
})

var pressButtonOnEnter = function(inputIdentifier, buttonIdentifier) {
    $(inputIdentifier).keypress(function(e){
      if (e.keyCode == 13) {
        $(buttonIdentifier).click()
      }
    })
}

var callGetMessagesEvery500Millis = function() {
	setInterval(getMessages, 500)
}

var currentUser = ''

var ajaxCall = function(props, handler) {
	$.ajax({
		url: props.address,
		success:function(data) {
			handler(data)
		}
	})
}

var newUser = function() {
	currentUser = $("#username").val()
	$('.user').hide()
	getMessages()
}

var getMessages = function() {
	ajaxCall({
		address: host + "getMessages/"
	}, function(data) {
		$('.messages').text(data)
	})
}

var sendMessage = function() {
	var inputField = $('#user-message')
	if (currentUser === '') {
		alert('Please create a new user first!')
	} else {
		ajaxCall({
			address: host + "sendMessage/?user=" + currentUser + "&message=" + inputField.val()
		}, function(data) {
			getMessages()
		})
	    inputField.val('')
	}
}